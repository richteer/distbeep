#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

typedef struct beep_s {
	float freq;
	int dur;
} beep_t;


int sfd = -1;

void handle_sigint(int x) { fprintf(stderr,"How dare you interrupt the music!\n"); close(sfd); exit(0); }

static int init(char* port);
static int dothething(void);
static int recvbeep(beep_t *b);
static int beep(beep_t *b);

int main(int argc, char** argv)
{
	if (argc != 2) {
		fprintf(stderr,"Usage: %s <port>\n",argv[0]);
		exit(1);
	}
	signal(SIGINT,handle_sigint);

	// Do network setup here
	if (init(argv[1])) return -1;

	dothething();

	return 0;
}

static int init(char* port)
{
	struct addrinfo hints, *servinfo, *p;
	int rv;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;

	if ((rv = getaddrinfo("0.0.0.0",0xbeeb,&hints,&servinfo)) != 0) {
		fprintf(stderr,"getaddrinfo: %s\n", gai_strerror(rv));
		return -2;
	}

	for (p = servinfo;
	     p != NULL;
		 p = p->ai_next)
	{
		if (-1 != (sfd = socket(p->ai_family, p->ai_family, p->ai_protocol)))
			break;
	}

	if (NULL == p) {
		fprintf(stderr,"Could not create socket!\n");
		return -3;
	}

	if (0 != bind(sfd,p->ai_addr,p->ai_addrlen)) {
		perror("bind");
		return -4;
	}
	freeaddrinfo(servinfo);

	return 0;
}

static int dothething(void)
{
	printf("Doing the thing...\n");
	int freq = 0;
	beep_t b;

	while (0 == (recvbeep(&b))) {
		beep(&b);
	}
	return 0;
}

static int recvbeep(beep_t *b)
{
	char buf[100] = {0};
	struct sockaddr fromaddr;
	socklen_t fromlen;
	int ret;
	unsigned long resp = 0xbeebbeebbeebbeeb;

doitagain:
	ret = recvfrom(sfd,buf,100,0,&fromaddr,&fromlen);

	if (-1 == ret) {
		perror("recv");
		return -1;
	}

	if (ret != sizeof(beep_t)) {
		fprintf(stderr,"Packet received not of the correct size");
		return -1;
	}

	memcpy(b,buf,sizeof(beep_t));

	if (*((unsigned long*) b) == 0) {
		printf("Received ping!\n");	
		if (0 > sendto(sfd,&resp,8,0,&fromaddr,fromlen)) {
			perror("Respond to ping error");
			return -2;
		}
		goto doitagain;
	}


	return 0;
}
	
static int beep(beep_t *b)
{
	char buf[100] = {0};
	snprintf(buf,99,"beep -f %f -l %d\n",*b);
	system(buf);
}
