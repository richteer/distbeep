import socket
import struct
import sys
import time
from netaddr import IPNetwork
import beepmus


def noteToFreq(note):
	return beepmus.notes[note]

# Send a ping to potential beepers, and wait for a response...maybe
def beepPing(addr,sock):
	#print("Pinging " + addr)
	
	try:
		sock.sendto(bytes(8),(addr,0xbeeb))
	except Exception as e:
		#print(e)
		#print("I dun goofed")
		return False

	try:
		data = sock.recvfrom(8)
	except:
		return False
	
	return data[0] == struct.pack("II",0xbeebbeeb,0xbeebbeeb)

	

# Find all the beeper daemons on the given subnet
def beepSweep(subnet):
	s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
	s.settimeout(0.02)
	s.bind(("0.0.0.0",0xbeeb))

	return [str(x) for x in IPNetwork(subnet) if beepPing(str(x),s)]

# Send the beeps
def dothething(slaves,tasks):
	pass



def main():
	print(noteToFreq("A#1"))

	beeps = beepmus.readBeepmus(sys.argv[2])

	targets = (beepSweep(sys.argv[1]))
	if len(targets) == 0:
		print("No beepable machines found!")
		exit(2)

	dothething(targets,beeps)


if __name__ == "__main__":
	if len(sys.argv) != 3:
		exit(1)

	main()
