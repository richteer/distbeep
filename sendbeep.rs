#[no_uv];

extern mod native;

use std::io::net::udp::UdpSocket;
use std::cast::transmute;


#[start]
fn main(argc: int, argv: **u8) -> int {
    do native::start(argc, argv) {
        let args = std::os::args();
        let addr = from_str(args[1]).unwrap();
        let data: (f32, i32) = (from_str(args[2]).unwrap(), from_str(args[3]).unwrap());
        let mut sock = UdpSocket::bind(from_str("0.0.0.0:0").unwrap()).unwrap();
        sock.sendto(unsafe { transmute::<(f32, i32), [u8, ..8]>(data) }, addr);
    }
}
