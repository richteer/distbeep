extern mod native;

use std::cast::transmute;
use std::io::net::udp::UdpSocket;

#[start]
fn main(argc: int, argv: **u8) -> int {
    do native::start(argc, argv) {
        let addr = from_str("0.0.0.0:48875").unwrap();
        let mut sock = UdpSocket::bind(addr).expect("couldn't bind to BEEBSOCK");
        let mut bytes = (0f32, 0i32);
        loop {
            match {
                let mref = unsafe { transmute::<&mut (f32, i32), &mut [u8, ..8]>(&mut bytes) };
                sock.recvfrom(mref.as_mut_slice())
            } {
                Some((len, addr)) => {
                    debug!("len is {}", len);
                    info!("got msg from {}, content {:?}", addr.to_str(), bytes);
                    if bytes == (0f32, 0i32) {
                        // ping
                        sock.sendto([0xEB,0xBE, 0xEB, 0xBE, 0xEB, 0xBE, 0xEB, 0xBE], addr);
                        continue;
                    }
                    // beep
                    let (freq, duration) = bytes;
                    let args = [~"beep", ~"-l", duration.to_str(), ~"-f", freq.to_str()];
                    debug!("running beep with {:?}", args);
                    let status = std::run::process_status("sudo", args);
                    match status {
                        Some(rt) => if !rt.success() { error!("beep failed with {:?}", rt) },
                        None     => fail!("beep not support!")
                    }
                },
                None => fail!("recvfrom none? not support!")
            }
        }
    }
}
